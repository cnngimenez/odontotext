/*
  Copyright (C) 2024  cnngimenez

  index-ui.js

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

var current_odontogram = null;

function value_to_colour(value) {
    switch (value) {
    case false:
        return 'white';
    case true:
        return 'black';
    default:
        return 'gray';
    }
}

function value_to_visibility(value) {
    if (value) {
        return '';
    } else {
        return 'hidden';
    }
}

function add_tooth(g_id, tooth, num, prefix) {
    const eodon = document.querySelector(g_id);
    const etemplate = document.querySelector('template#tdiente');
    const etooth = etemplate.content.cloneNode(true);

    var elt = etooth.querySelector('g#d');
    elt.setAttribute('id', 'd-' + prefix + '-' + num);

    const x = (num % 18) * 125;
    var y = 0;
    
    if (num >= 18) {
        y = 170;
    }

    elt.setAttribute('transform', 'translate(' + x + ',' + y + ')');

    elt = etooth.querySelector('rect#dselect');
    elt.setAttribute('id', 'dselect-' + prefix + '-' + num);
    
    elt = etooth.querySelector('polygon#dup');
    elt.setAttribute('id', 'dup-' + prefix + '-' + num);
    elt.setAttribute('fill', value_to_colour(tooth.up));
        
    elt = etooth.querySelector('polygon#dright');
    elt.setAttribute('id', 'dright-' + prefix + '-' + num);
    elt.setAttribute('fill', value_to_colour(tooth.right));
    
    elt = etooth.querySelector('polygon#ddown');
    elt.setAttribute('id', 'ddown-' + prefix + '-' + num);
    elt.setAttribute('fill', value_to_colour(tooth.down));
    
    elt = etooth.querySelector('polygon#dleft');
    elt.setAttribute('id', 'dleft-' + prefix + '-' + num);
    elt.setAttribute('fill', value_to_colour(tooth.left));
    
    elt = etooth.querySelector('polygon#dcenter');
    elt.setAttribute('id', 'dcenter-' + prefix + '-' + num);
    elt.setAttribute('fill', value_to_colour(tooth.center));

    elt = etooth.querySelector('#dcrown');
    elt.setAttribute('id', 'dcrown-' + prefix + '-' + num);
    elt.setAttribute('visibility', value_to_visibility(tooth.crown));

    elt = etooth.querySelector('#dextracted');
    elt.setAttribute('id', 'dextracted-' + prefix + '-' + num);
    elt.setAttribute('visibility', value_to_visibility(tooth.extracted));
    
    elt = etooth.querySelector('text#dnum');
    elt.setAttribute('id', 'dnum-' + prefix + '-' + num);
    elt.innerHTML = num + 1;
    
    eodon.append(etooth);
    
    return etooth;
}

/**
 Update the tooth.
 */
function update_tooth(g_id, tooth, num, prefix) {
    const eodon = document.querySelector(g_id);
    const etooth = eodon.querySelector('g#d-' + prefix + '-' + num);
    if (etooth == undefined || etooth == null) {
        return false;
    }

    var elt = etooth.querySelector('polygon#dup-' + prefix + '-' + num);    
    elt.setAttribute('fill', value_to_colour(tooth.up));
        
    elt = etooth.querySelector('polygon#dright-' + prefix + '-' + num);
    elt.setAttribute('fill', value_to_colour(tooth.right));
    
    elt = etooth.querySelector('polygon#ddown-' + prefix + '-' + num);
    elt.setAttribute('fill', value_to_colour(tooth.down));
    
    elt = etooth.querySelector('polygon#dleft-' + prefix + '-' + num);
    elt.setAttribute('fill', value_to_colour(tooth.left));
    
    elt = etooth.querySelector('polygon#dcenter-' + prefix + '-' + num);
    elt.setAttribute('fill', value_to_colour(tooth.center));

    elt = etooth.querySelector('#dcrown-' + prefix + '-' + num);
    elt.setAttribute('visibility', value_to_visibility(tooth.crown));
    
    elt = etooth.querySelector('#dextracted-' + prefix + '-' + num);
    elt.setAttribute('visibility', value_to_visibility(tooth.extracted));

    elt = etooth.querySelector('text#dnum-' + prefix + '-' + num);
    elt.innerHTML = num + 1;
}


function update_odontogram_view(text) {
    update_odontogram(text, 'g#odontogram-view', 'view');
}

function update_odontogram_edit(text) {
    update_odontogram(text, 'g#odontogram-edit', 'edit');
}

/**
 Update the odontogram with the given text.

 It does not create any elements. Just updates the existing ones.
*/
function update_odontogram(text, g_id, prefix) {
    if (text != undefined && text != false) {
        current_odontogram = string_to_odontogram(text);
    }

    current_odontogram.forEach( (elt, index) => {
        update_tooth(g_id, elt, index, prefix);
    });
    
    refresh_svg();
}

function update_odontogram_size(width, height) {
    const eodon = document.querySelector('g#odontogram-edit');
}

/**
 SVG would not update itself except on some specific functions. For example,
 adding a new tag would not appear.

 This function force the SVG updates by trying to trigger a modification on the
 parent div tag.
 */
function refresh_svg() {
    var ediv = document.querySelector('#dodon-edit');
    if (ediv != null) {
        ediv.innerHTML += '';
        assign_odontogram_events();
    }
    ediv = document.querySelector('#dodon-view');
    if (ediv != null) {
        ediv.innerHTML += '';
    }
}

function create_odontogram_view(text) {
    create_odontogram(text, 'g#odontogram-view', 'view');
}

function create_odontogram_edit(text) {
    create_odontogram(text, 'g#odontogram-edit', 'edit');
}

/**
 Create the odontogram from the given text.

 This means, actually create the SVG elements and add it to the svg tag.

 @param g_id [String] The g tag id to add each tooth.
 */
function create_odontogram(text, g_id, prefix) {
    current_odontogram = string_to_odontogram(text);

    current_odontogram.forEach( (elt, index) => {
        add_tooth(g_id, elt, index, prefix);
    });

    refresh_svg();
}

function odontogram_select_tooth(num, prefix) {
    current_odontogram.forEach( (elt, num) => {
        document.querySelector('rect#dselect-' + prefix + '-' + num).setAttribute('class', 'unselected');
    });
    document.querySelector('rect#dselect-' + prefix + '-' + num).setAttribute('class', 'selected');
}
// --------------------------------------------------
// Tooth Editor

var current_tooth = {
    up: false,
    down: false,
    left: false,
    right: false,
    center: false,
    num: 1
}

function create_tootheditor() {
    const eodon = document.querySelector('g#tooth');
    const etemplate = document.querySelector('template#tdiente');
    const etooth = etemplate.content.cloneNode(true);    

    var elt = etooth.querySelector('g#d');
    elt.setAttribute('id', 'dedit');
   
    elt = etooth.querySelector('polygon#dup');
    elt.setAttribute('id', 'deditup');
    elt.setAttribute('fill', value_to_colour(current_tooth.up));
        
    elt = etooth.querySelector('polygon#dright');
    elt.setAttribute('id', 'deditright');
    elt.setAttribute('fill', value_to_colour(current_tooth.right));
    
    elt = etooth.querySelector('polygon#ddown');
    elt.setAttribute('id', 'deditdown');
    elt.setAttribute('fill', value_to_colour(current_tooth.down));
    
    elt = etooth.querySelector('polygon#dleft');
    elt.setAttribute('id', 'deditleft');
    elt.setAttribute('fill', value_to_colour(current_tooth.left));
    
    elt = etooth.querySelector('polygon#dcenter');
    elt.setAttribute('id', 'deditcenter');
    elt.setAttribute('fill', value_to_colour(current_tooth.center));

    elt = etooth.querySelector('text#dnum');
    elt.setAttribute('id', 'deditnum');
    elt.innerHTML = current_tooth.num + 1;
    
    eodon.append(etooth);
    // assign_tootheditor_events();
    
    return etooth;
}

function update_tootheditor() {
    const etooth = document.querySelector('g#dedit');

    var elt = etooth.querySelector('polygon#deditup');
    elt.setAttribute('fill', value_to_colour(current_tooth.up));

    elt = etooth.querySelector('polygon#deditleft');
    elt.setAttribute('fill', value_to_colour(current_tooth.left));

    elt = etooth.querySelector('polygon#deditdown');
    elt.setAttribute('fill', value_to_colour(current_tooth.down));

    elt = etooth.querySelector('polygon#deditright');
    elt.setAttribute('fill', value_to_colour(current_tooth.right));
    
    elt = etooth.querySelector('polygon#deditcenter');
    elt.setAttribute('fill', value_to_colour(current_tooth.center));

    elt = etooth.querySelector('g#dcrown');
    elt.setAttribute('visibility', value_to_visibility(current_tooth.crown));

    elt = etooth.querySelector('g#dextracted');
    elt.setAttribute('visibility', value_to_visibility(current_tooth.extracted));
    
    elt = etooth.querySelector('text#deditnum');
    elt.innerHTML = current_tooth.num + 1;

    document.querySelector('#dtoothtext').innerHTML =
        dent_to_char(current_tooth);
    
    document.querySelector('div#dtooth').innerHTML += ''; // <- force to update the SVG.    
    assign_tootheditor_events(); // <- events were erased by the last sentence.
}

function edit_tooth(num) {
    current_tooth = current_odontogram[num];
    current_tooth.num = num;
    update_tootheditor();
    odontogram_select_tooth(num, 'edit');
}

// --------------------------------------------------
// OdontoText

/**
 Update the text area with the provided or the current odontotext.
 
 @param odontotext [String] The text to change the textarea and the URL. If null or not provided, the current odontogram is used.
 @param selector [String] The textarea querySelector.
 */
function update_odontotext(odontotext, selector){
    if (odontotext == undefined || odontotext == null) {
        odontotext = odontogram_to_string(current_odontogram);
    }
    document.querySelector(selector).value = odontotext;
}

// --------------------------------------------------
// Events

function assign_tootheditor_events() {
    document.querySelector('polygon#deditcenter')
        .addEventListener('click', () => {tootheditor_onclick('center')});
    document.querySelector('polygon#deditup')
        .addEventListener('click', () => {tootheditor_onclick('up')});
    document.querySelector('polygon#deditleft')
        .addEventListener('click', () => {tootheditor_onclick('left')});
    document.querySelector('polygon#deditdown')
        .addEventListener('click', () => {tootheditor_onclick('down')});
    document.querySelector('polygon#deditright')
        .addEventListener('click', () => {tootheditor_onclick('right')});
}

function tootheditor_onclick(position) {
    current_tooth[position] = !current_tooth[position];
    current_odontogram[current_tooth.num][position] = current_tooth[position];
    
    update_tootheditor();
    update_odontogram_edit();
    update_odontotext(null, 'textarea#odontotext-edit');
    update_odontourl('#odontourl-edit');
}

function crown_onclick() {
    // Hopefully current_tooth.crown will never be nil... hopefully!
    current_tooth.crown = !current_tooth.crown;
    current_odontogram[current_tooth.num].crown = current_tooth.crown;
    
    update_tootheditor();
    update_odontogram_edit();
    update_odontotext(null, 'textarea#odontotext-edit');
    update_odontourl('#odontourl-edit');    
}

function extracted_onclick() {
    // Hopefully current_tooth.extracted will never be nil... hopefully!
    current_tooth.extracted = !current_tooth.extracted;
    current_odontogram[current_tooth.num].extracted = current_tooth.extracted;
    
    update_tootheditor();
    update_odontogram_edit();
    update_odontotext(null, 'textarea#odontotext-edit');
    update_odontourl('#odontourl-edit');    
}

function assign_odontogram_events() {
    current_odontogram.forEach( (tooth, num) => {
        document.querySelector('g#d-edit-' + num).addEventListener('click', () => {
            odontogram_tooth_onclick(num, 'edit');
        });
    });
}

function odontogram_tooth_onclick(num) {
    console.log('Editando: ' + num)
    edit_tooth(num);
}

function odontourl_copy_onclick(selector) {
    const odontourl = document.querySelector(selector).attributes['href'];

    try {
        navigator.clipboard.writeText(odontourl).then(
            () => {
                ons.notification.toast('¡URL copiada!', {timeout: 2000});
            },
            (err) => {
                ons.notification.toast('No se pudo copiar la URL. Intente manualmente.', {timeout: 2000});
            });
    } catch {
        ons.notification.toast('No se pudo copiar la URL. Intente manualmente.', {timeout: 2000});
    }
}

function odontotext_copy_onclick(selector) {
    const text = document.querySelector(selector).value;

    try {
        navigator.clipboard.writeText(text).then(
            () => {
                ons.notification.toast('¡URL copiada!', {timeout: 2000});
            },
            (err) => {
                ons.notification.toast('No se pudo copiar la URL. Intente manualmente.', {timeout: 2000});
            });
    } catch {
        ons.notification.toast('No se pudo copiar la URL. Intente manualmente.', {timeout: 2000});
    }
}

function odontotext_onchange() {
    const str = document.querySelector('textarea#odontotext-edit').value;
    update_odontogram_edit(str);
    update_odontourl('#odontourl-edit');
}

function window_onresize() {
    const height = window.innerHeight;
    const width = window.innerWidth;

    update_odontogram_size(width, height);
}

function edit_onclick() {
    const enavigator = document.querySelector('#navigator');
    enavigator.pushPage('page-main.html').then( () => {
        show_edit_page();
    });
}

function help_onclick() {
    show_help_page();
}

/**
 Assign events to all the elements on index.html.
 */
function assign_events_editor() {
    window.addEventListener('resize', () => {window_onresize()});
    document.querySelector('textarea#odontotext-edit')
        .addEventListener('keyup', () => {odontotext_onchange()});
    document.querySelector('ons-back-button').options.callback = () => {
        update_viewer();
    };
}

function assign_events_viewer() {
}

// --------------------------------------------------
// URL handler

/**
 Return the odontotext from the URL.
 */
function url_get_odontotext() {
    const url = new URL(document.URL);
    
    return url.searchParams.get('odontotext');
}

// --------------------------------------------------
// OdontoURL

function get_odontourl(text) {
    let url = new URL(document.URL);
    url.searchParams.set('odontotext', text);
    
    return url;
}

function update_odontourl(selector) {
    const eanchor = document.querySelector(selector);
    const odontotext = odontogram_to_string(current_odontogram);
    const url = get_odontourl(odontotext);
    
    eanchor.setAttribute('href', url);
    eanchor.innerHTML = url;
}

// --------------------------------------------------
// Download anchor

/**
 Update the download anchor data with the odontogram-view SVG.
 */
function update_download_anchor() {
    const esvg = document.querySelector('#dodon-view svg');
    const eanchor = document.querySelector('a#download-view');
    
    eanchor.href='data:image/svg+xml;utf8,' + esvg.outerHTML;
}

// --------------------------------------------------
// Startup

function show_edit_page() {
    odontotext = odontogram_to_string(current_odontogram);
    
    create_odontogram_edit(odontotext);
    
    update_odontotext(null, 'textarea#odontotext-edit');
    update_odontourl('#odontourl-edit');
    
    create_tootheditor();
    edit_tooth(0);

    assign_events_editor();
}

function startup_editor() {    
    const odontotext = url_get_odontotext();
    create_odontogram_edit(odontotext);
    update_odontotext(null, 'textarea#odontotext-edit');
    update_odontourl('#odontourl-edit');

    create_tootheditor();
    edit_tooth(0);

    assign_events_editor();
}

function update_viewer() {
    update_odontogram_view();

    update_download_anchor();
    update_odontotext(null, 'textarea#odontotext-view');
    update_odontourl('#odontourl-view');

    assign_events_viewer();
}

/**
 Creates viewer elements.

 To avoid creation of elements on the DOM use udpate_viewer();
 */
function startup_viewer() {
    const odontotext = url_get_odontotext();
    create_odontogram_view(odontotext);
    
    update_download_anchor();
    update_odontotext(null, 'textarea#odontotext-view');    
    update_odontourl('#odontourl-view');

    assign_events_viewer();
}

function startup() {
    startup_viewer();
}

function show_help_page() {
    const enavigator = document.querySelector('#navigator');
    enavigator.pushPage('page-help.html');
}

ons.ready(startup);
