/*
  Copyright (C) 2024  cnngimenez

  odonchar.js

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const alphabet = construct_alphabet();

function construct_alphabet() {
    let alpha = [];

    // 0-1
    for (i = 48; i <= 57; i++) {
        alpha.push(String.fromCharCode(i));
    }
    // A-Z
    for (i = 65; i <= 90; i++) {
        alpha.push(String.fromCharCode(i));
    }
    // a-z
    for (i = 97; i <= 122; i++) {
        alpha.push(String.fromCharCode(i));
    }

    return alpha;
}

function char_to_num(char) {
    return alphabet.indexOf(char);
}

function char_to_dent(char) {
    const num = char_to_num(char);

    if (num < 0) {
        return false;
    }

    return {
        up:     (num & 0b00001) > 0,
        left:   (num & 0b00010) > 0,
        down:   (num & 0b00100) > 0,
        right:  (num & 0b01000) > 0,
        center: (num & 0b10000) > 0,
        crown: (num == 32),
        extracted: (num == 33)
    }
}

function dent_to_char(tooth) {
    var num = 0;

    if (tooth.up) {
        num |= 0b00001;
    }
    if (tooth.left) {
        num |= 0b00010;
    }
    if (tooth.down) {
        num |= 0b00100;
    }
    if (tooth.right) {
        num |= 0b01000;
    }
    if (tooth.center) {
        num |= 0b10000;
    }
    if (tooth.crown) {
        num = 32;
    }
    if (tooth.extracted) {
        num = 33;
    }

    return alphabet[num];
}

function string_to_odontogram(str) {
    var str2 = "";
    if (str != null) {
        str2 = str;
    }
    const arr = Array.from(str2);
    const strlength = arr.length;

    for (i = 1; i <= 36 - strlength; i++) {
        arr.push('0');
    }

    return arr.map( (char) => char_to_dent(char) );
}

function odontogram_to_string(odontogram) {
    return odontogram.map( (dent) => dent_to_char(dent) ).join("");
}
